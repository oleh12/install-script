#!/usr/bin/env bash

echo "install Homebrew"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

#-----=====brew install=====-----

echo "java"
brew install htop 

echo "zsh"
brew install zsh

#-----=====brew cask install=====-----

echo "android-file-transfer"
brew cask install android-file-transfer

echo "java"
brew cask install java

echo "google-chrome"
brew cask install google-chrome

echo "sublime-text"
brew cask install sublime-text

echo "vlc"
brew cask install vlc

echo "intellij-idea"
brew cask install intellij-idea

echo "skype"
brew cask install skype

echo "transmission"
brew cask install transmission

echo "iterm2"
brew cask install iterm2

echo "spotify"
brew cask install spotify

echo "bitcoin-core"
brew cask install bitcoin-core

echo "litecoin"
brew cask install litecoin



